TAXONOMY SIFTER
===============

Taxonomy Sifter <http://drupal.org/project/taxonomy_sifter> is a Drupal module
that provides a block with terms which any or all selected must apply to a node
for it to be listed when viewing a taxonomy term page, e.g. taxonomy/term/...
The vocabularies used in the block is configurable.

See the module's handbook page <http://drupal.org/node/143089> for information
on how to install, configure and use the module.

Taxonomy Sifter is developed by
  
  * Thomas Barregren <http://drupal.org/user/16678>
  * Nancy Wichmann <http://drupal.org/user/101412>

The development has been sponsored by

  * SSPA Sweden <http://www.sspa.se/>
  * imBridge <http://drupal.org/node/131670>
