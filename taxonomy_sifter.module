<?php

/**
 * @file
 * Taxonomy Sifter - provides a block for selecting a term to filter nodes.
 *
 * Authors:
 *   Thomas Barregren <http://drupal.org/user/16678>.
 *   Nancy Wichmann <http://drupal.org/user/101412>.
 *
 * Sponsors:
 *   SSPA Sweden <http://www.sspa.se/>
 *   imBridge <http://www.imbridge.com/>
 */

/**
 * Implementation of hook_perm().
 */
function taxonomy_sifter_perm() {
  return array('administer taxonomy sifter', 'view sifter block');
}

/**
 * Implementation of hook_menu().
 */
function _taxonomy_sifter_menu() {
  $items = array();
  $items['taxonomy/term'] = array(
    'type' => MENU_CALLBACK,
    'page callback' => 'taxonomy_sifter_taxonomy_term_filter',
    'access arguments' => array('access content'),
    );
  return $items;
}

function taxonomy_sifter_menu_alter(&$items) {
  $items['taxonomy/term/%']['page callback'] = 'taxonomy_sifter_taxonomy_term_filter';
  unset($items['taxonomy/term/%']['file']);
}

/**
 * Implementation of hook_block().
 */
function taxonomy_sifter_block($op = 'list', $delta = 0, $edit = array()) {
  $function = "_taxonomy_sifter_block_$op";
  if (function_exists($function)) {
    return $function($delta, $edit);
  }
  else { drupal_set_message("$function does not exist."); }
}

/**
 * Implementation of hook_help().
 */
function taxonomy_sifter_help($path, $args) {
  switch ($path) {
    case 'admin/modules#description':
      return t('Provides a block with terms which <em>any</em> or <em>all</em> selected must apply to a node for it to be listed when viewing a taxonomy term page, e.g. <code>taxonomy/term/...</code>');

    case 'admin/help#taxonomy_sifter':
      return t('<p><a href="http://drupal.org/project/taxonomy_sifter">Taxonomy Sifter</a> provides a block where the user can select terms that will automatically be combined with the terms in every request of the form <code>taxonomy/term/...</code> for the duration of their session. The selected terms are combined with the same operator already used in the request. If no operator is used, that is a single term is given, the sifter uses the operator given in the module\'s settings. The vocabularies of the terms presented in the sifter block is also configurable. For further documentation, see the <a href="http://drupal.org/node/22273">handbook</a>.</p>');
  }
}

/**
 * BLOCK API
 */

/**
 * Blocks are listed.
 */
function _taxonomy_sifter_block_list() {
  $blocks[0]['info'] = t('Taxonomy Sifter');
  return $blocks;
}

/**
 * Blocks are configured.
 */
function _taxonomy_sifter_block_configure($delta, $edit) {
  drupal_add_css(drupal_get_path('module', 'taxonomy_sifter') .'/taxonomy_sifter.css');
  $form = array('#attributes' => array('class' => 'taxonomy-sifter'));
  // Check that the user has permission to use the taxonomy sifter.
  if (!user_access('administer taxonomy sifter')) {
    $form['taxonomy_sifter'] = array(
      '#type' => 'markup',
      '#value' => '<p>'. t('You do not have permission to use the taxonomy sifter.') .'</p>',
    );
    return $form;
  }

  // Get all available vocabularies.
  $vocabularies = _taxonomy_sifter_db_get_vocabularies();

  // Show checkboxes with roles that can be delegated if any.
  if ($vocabularies) {
    $form['taxonomy_sifter'] = array(
      '#type' => 'fieldset',
      '#title' => t('Taxonomy Sifter Settings'),
      '#collapsible' => TRUE,
      '#collapsed' => FALSE,
      );

    $form['taxonomy_sifter']['taxonomy_sifter_vocabularies'] = array(
      '#type' => 'checkboxes',
      '#title' => t('Vocabularies'),
      '#options' => $vocabularies,
      '#default_value' => _taxonomy_sifter_variable_vocabularies(),
      '#description' => t('Select vocabularies that should be available in the Taxonomy Sifter block.'),
      '#prefix' => '<div class="taxonomy-sifter-checkboxes">',
      '#suffix' => '</div>',
      );

    $form['taxonomy_sifter']['taxonomy_sifter_operator'] = array(
      '#type' => 'radios',
      '#title' => t('Default operator'),
      '#options' => array(t('or (any term)'), t('and (all terms)')),
      '#default_value' => _taxonomy_sifter_variable_operator(),
      '#description' => t('Select the default term filter. When a taxonomy term page or feed is requested, the given terms are combined with those selected in the Taxonomy Sifter block. If more than one term is given in the request, the combination is done with the operator used in the request. If only one term is given, the operator selected above is used.'),
      '#prefix' => '<div class="taxonomy-sifter-radios">',
      '#suffix' => '</div>',
      );

    $display_opts = array(
      'select' => t('Select list'),
      'checkboxes' => t('Check boxes'),
      'buttons' => t('Buttons'),
      );
    $form['taxonomy_sifter']['taxonomy_sifter_select_type'] = array(
      '#type' => 'radios',
      '#title' => t('Display choices as'),
      '#options' => $display_opts,
      '#default_value' => variable_get('taxonomy_sifter_select_type', 'select'),
      '#description' => t('This determines how the possible choices will be shown.'),
      '#prefix' => '<div class="taxonomy-sifter-radios">',
      '#suffix' => '</div>',
      );
  }
  else {
    $form['taxonomy_sifter'] = array(
      '#type' => 'markup',
      '#value' => '<p>'. t('No vocabulary is available.') .'</p>',
    );
  }

  return $form;
}

/**
 * Save block setings.
 */
function _taxonomy_sifter_block_save($delta, $edit) {
  _taxonomy_sifter_variable_vocabularies($edit['taxonomy_sifter_vocabularies']);
  _taxonomy_sifter_variable_operator($edit['taxonomy_sifter_operator']);
  variable_set('taxonomy_sifter_select_type', $edit['taxonomy_sifter_select_type']);
}

/**
 * Block is viewed.
 */
function _taxonomy_sifter_block_view($vid) {
  if (user_access('view sifter block') && arg(0) == 'taxonomy' && arg(1) == 'term') {
    drupal_add_css(drupal_get_path('module', 'taxonomy_sifter') .'/taxonomy_sifter.css');
    $vocabularies = _taxonomy_sifter_db_get_vocabularies();
    $block = array(
      'content' => drupal_get_form('_taxonomy_sifter_block_form', $vid),
    );
    return $block;
  }
}

/**
 * BLOCK CONTENT AND FORM PROCESSING
 */

/**
 * Returns the themed content of the taxonomy sifter block of $vid.
 */
function _taxonomy_sifter_block_form($form, $vid) {
  $form = array();

  // Submit and reset buttons
  $form['buttons']['#weight'] = 99;
  $form['buttons']['submit'] = array('#type' => 'submit', '#value' => t('Sift'));
  $form['buttons']['reset'] = array('#type' => 'submit', '#value' => t('Reset'));

  // Get the names of all vocabularies.
  $vocabularies = _taxonomy_sifter_db_get_vocabularies();

  // Get the available vocabularies.
  $vids = _taxonomy_sifter_variable_vocabularies();
  $vids = array_filter($vids);

  $curr_selection = _taxonomy_sifter_session_get_terms();
  $curr_selection = $curr_selection[0];

  drupal_set_message(print_r($x, true));
  $select_type = variable_get('taxonomy_sifter_select_type', 'select');
  $current = taxonomy_get_term(arg(2));
  $curr_tid = $current->tid;
  $oper = variable_get('taxonomy_sifter_operator', 0);

  // For each vocabulary, add a select element of its terms.
  foreach ($vids as $vid) {
    // Build a list of terms to be presented in the select element.
    $options = array();
    $tree = taxonomy_get_tree($vid);
    foreach ($tree as $term) {
      if ($oper == 0) {
        $sql = 'SELECT COUNT(DISTINCT(n.nid)) FROM {node} n INNER JOIN {term_node} tn ON n.vid = tn.vid WHERE tn.tid IN (%d,%d) AND n.status = 1';
      }
      else {
        $sql = 'SELECT COUNT(DISTINCT(n.nid)) FROM {node} n INNER JOIN {term_node} tn1 ON n.vid = tn1.vid INNER JOIN {term_node} tn2 ON n.vid = tn2.vid WHERE tn1.tid = %d AND tn2.tid = %d AND n.status = 1';
      }
      $count = db_result(db_query($sql, $curr_tid, $term->tid));
      if ($count > 0) {
        $options[$term->tid] = str_repeat($term->depth, '-') . $term->name;
      }
    }

    // Build and return the select element.
    switch ($select_type) {
      case 'checkboxes':
      case 'select':
        $form["taxonomy_sifter_terms_$vid"] = array(
          '#title' => t('Sift by @vid', array('@vid' => $vocabularies[$vid])),
          '#type' => $select_type,
          '#multiple' => TRUE,
          '#options' => $options,
          '#default_value' => _taxonomy_sifter_session_get_terms($vid),
          );
        break;

      case 'buttons':
        foreach ($options as $tid => $name) {
          $class = 'taxonomy-sifter-term-button';
          if ($curr_selection == $tid) {
            $class .= ' active';
          }
          $form[$name] = array(
            '#type' => 'submit',
            '#value' => check_plain($name),
            '#attributes' => array('class' => $class),
            );
        }
        unset($form['buttons']['submit']);
    }
  }

  return $form;
}

/**
 * Process the submited form.
 */
function _taxonomy_sifter_block_form_submit($form, &$form_state) {
  // @TODO: Change to dedicated functions from form.
  $op = isset($form_state['values']['op']) ? $form_state['values']['op'] : '';
  switch ($op) {
    case t('Sift'):
      // Clear away extra stuff in values.
      foreach ($form_state['values'] as $name => $value) {
        if (drupal_substr($name, 0, 22) != 'taxonomy_sifter_terms_') {
          unset($form_state['values'][$name]);
        }
      }
      _taxonomy_sifter_session_set_terms($form_state['values']);
      break;

    case t('Reset'):
      _taxonomy_sifter_session_set_terms(array());
      break;

    default:
      // $op is term name on select type = button.
      // But the value may have leading '-' due to term hierarchy.
      $op = trim($op, '- ');
      $term = taxonomy_get_term_by_name($op);
      $_SESSION['taxonomy_sifter_terms']['taxonomy_sifter_terms_'.$term[0]->vid] = array($term[0]->tid => $term[0]->tid);
  }
}

/**
 * FILTERING BY TAXONOMY
 */

/**
 * Intercept the 'taxonomy/term' callback function to complement the terms' id
 * in the URL with those of the terms selcted in the block.
 */
function taxonomy_sifter_taxonomy_term_filter($terms, $depth = 0, $feed = 'page') {
  $terms = _taxonomy_sifter_rewrite_filter($terms);
  module_load_include('inc', 'taxonomy', 'taxonomy.pages');
  return taxonomy_term_page($terms, $depth, $feed);
}

/**
 * Rewrite the taxonomy filter.
 */
function _taxonomy_sifter_rewrite_filter($filter) {
  $original = $filter;
  // Get the terms selected in the block.
  $terms = _taxonomy_sifter_session_get_terms();

  // If terms are selected, add them to the filter.
  if ($terms) {
    // Extract the terms already in the filter.
    // Note 1: Only one kind of taxonomy operators + and , can be used in the
    // URL. Therefore we must use the same operator as already in the URL. If
    // no such operator is avaiable, we use the configured default operator.
    // Note 2: a plus sign in the URL is converted to space.
    $op_lut = array(' ', ',');
    $op_def = _taxonomy_sifter_variable_operator();
    $op = $op_lut[$op_def];
    $filter = explode($op, $filter);
    if (count($filter) == 1) {
      // The default operator wasn't used in the filter.
      // Swap operator and try again.
      $op = $op_lut[($op_def + 1) % 2];
      $filter = explode($op, $filter[0]);
      if (count($filter) == 1) {
        // The other operator wasn't used either.
        // Swap back to the default operator.
        $op = $op_lut[$op_def];
      }
    }

    // Add the terms selected in the block to the terms already in the filter.
    $filter = array_merge($filter, $terms);
    $filter = array_flip(array_flip($filter));  // array_unique() is overkill

    // Build the filter string
    $filter = implode($op, $filter);
  }

  return $filter;
}

/**
 * DATABASE
 */

/**
 * Get available vocabularies.
 */
function _taxonomy_sifter_db_get_vocabularies() {
  static $vocabularies;
  if (!isset($vocabularies)) {
    $vocabularies = array();
    $result = db_query('SELECT vid, name FROM {vocabulary} ORDER BY weight, name');
    while ($vocabulary = db_fetch_object($result)) {
      $vocabularies[$vocabulary->vid] = $vocabulary->name;
    }
  }
  return $vocabularies;
}

/**
 * PERSISTENT VARIABLES
 */

/**
 * Sets and gets selected vocabularies.
 */
function _taxonomy_sifter_variable_vocabularies($vocabularies = NULL) {
  return _taxonomy_sifter_variable('taxonomy_sifter_vocabularies', $vocabularies, array());
}

/**
 * Sets and gets selected operator.
 */
function _taxonomy_sifter_variable_operator($operator = NULL) {
  return _taxonomy_sifter_variable('taxonomy_sifter_operator', $operator, 0);
}

/**
 * Sets and gets the named persisted variable.
 */
function _taxonomy_sifter_variable($name, $value = NULL, $default = NULL) {
  if (isset($value)) {
    variable_set($name, $value);
  }
  return variable_get($name, $default);
}

/**
 * SESSION VARIABLES
 */

/**
 * Returns the selected terms.
 */
function _taxonomy_sifter_session_get_terms($vid = NULL) {
  if (is_null($vid)) {
    $terms = array();
    if (isset($_SESSION['taxonomy_sifter_terms'])) {
      $terms = $_SESSION['taxonomy_sifter_terms'];
    }
    if ($terms) {
      $terms = call_user_func_array('array_merge', $terms); // Flattens the array.
    }
  }
  else {
    if (!isset($_SESSION['taxonomy_sifter_terms']["taxonomy_sifter_terms_$vid"])) {
      $_SESSION['taxonomy_sifter_terms']["taxonomy_sifter_terms_$vid"] = array();
    }
    return $_SESSION['taxonomy_sifter_terms']["taxonomy_sifter_terms_$vid"];
  }
  return $terms;
}

/**
 * Set the selected terms.
 */
function _taxonomy_sifter_session_set_terms($terms = array()) {
  $_SESSION['taxonomy_sifter_terms'] = $terms;
}
